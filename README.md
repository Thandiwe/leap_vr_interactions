###Leap Interactions in VR###

This project if for AR User Interface interaction simulation in Virtual Reality using a leap motion controller for hand tracking and gesture recognition
 
### Getting Started ###
The following will be required:

* Unity version  5.5.0  
* HTC Vive
* Leap motion controller device , for more information on this device please visit https://www.leapmotion.com/

### The ExperimentLab Scene ###

* In the Assets there is an ExperimentLab folder which contains the ExperimentLab Scene
* The Scene comparises of 4 tables with 6 interactive objects which can be (a) picked up by performing a pinch gesture (b) rotated by using two closed fists  and (c) a thumbs up sign to confirm placement
* The task is to pic up an interactive object at place it on to the translucent highlighted shapes as accurately as possible.

### Contact ###
Thandiwe Feziwe Mangana

*  Email: feziwemangana@gmail.com