﻿#pragma strict

var ghost : GameObject;

function Start () {
	
}

function Update () {
	
}

function trigger() {
	    var posDeviation = calculatePosDeviation();
        var rotDeviation = calculateRotDeviation();
		return [posDeviation, rotDeviation];
}


function calculatePosDeviation(){
	var ghostPos = ghost.transform.position * 100.0f;
	var selecPos = transform.position * 100.0f;
	print("ghostPosition: " + ghostPos);
	print("targetPosition: " + selecPos);
	return new Vector3(Mathf.Abs(ghostPos.x - selecPos.x), Mathf.Abs(ghostPos.y - selecPos.y), Mathf.Abs(ghostPos.z - selecPos.z));
	//return Vector3.Distance(ghostPos, selecPos);
}

function calculateRotDeviation(){
	//var ghostRot = ghost.transform.rotation.eulerAngles;
	//var selecRot = selectable.transform.rotation.eulerAngles;
	var ghostRot = ghost.transform.rotation;
	var selecRot = transform.rotation;
	var relativeRot = Quaternion.Inverse(ghostRot) * selecRot;

	var relativeAngles = relativeRot.eulerAngles;

	return correctAmbigousAngles(relativeAngles);

	//var res = ghostRot - selecRot;
	// Calculate modulo 360 elementwise onf the res vector3
	//res = Mathf.Abs(res) - Vector3(360, 360, 360);
	//return relativeAngles;
}

function correctAmbigousAngles(angles : Vector3){
	print("Angles before correction:" + angles);
	var correctedX : float = correctAngle(angles.x);
	var correctedY : float = correctAngle(angles.y);
	var correctedZ : float = correctAngle(angles.z);
	return new Vector3(correctedX, correctedY , correctedZ);
}

function correctAngle(angle : float){
	//print("correcting angle: " + angle);
	var angleMod = angle % 360;
	if(angleMod <= 180){
		//print("Angle smaller 180.");
		return angleMod;
	} else {
	    // if angleMod is >180° it was turned past the axis of symmetry.
	    // We want to have the absolute deviation from the optimal orientation therfore the substraction
		//print("Angle greater 180.");
		return 360 - angleMod;
	}
}