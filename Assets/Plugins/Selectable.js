﻿#pragma strict

var focused = false;
var selected = false;


var rotationApplied = Vector3(0, 0, 0);

var lastPositionalDev = Vector3(-1000, -1000, -1000);
var lastRotationalDev = Vector3(-1000, -1000, -1000);

function Start () {
	
}

function Update () {
	
}


function focus(anySelected){
	focused = true;
	if(! anySelected){
		var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.yellow;
    }
}

function unfocus(){
	focused = false;
	if(! selected){
		var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.white;
    }
}

function ApproveSelection(){
	if(focused){
		selected = true;
		var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.magenta;
        var rgBody = gameObject.GetComponent.<Rigidbody>();
        rgBody.useGravity = false;
        rgBody.isKinematic = true;
	}
}

function Deselect(){
	selected = false;
	var rgBody = gameObject.GetComponent.<Rigidbody>();
    rgBody.useGravity = true;
   	rgBody.isKinematic = false;

   	//calculate the distance and rotational deviation in relation to ghost
   	var targetCollision = gameObject.GetComponent.<TargetCollision>();
	var deviations = targetCollision.trigger();
	lastPositionalDev = deviations[0];
	lastRotationalDev = deviations[1];
	print("Positional deviation: " + lastPositionalDev);
	print("Rotational deviation: " + lastRotationalDev);
	print("Total rotation: " + rotationApplied);
	//TODO: record the deviations for analysis
}