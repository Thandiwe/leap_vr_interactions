﻿
// Use this for initialization
function Start () {
}

// Update is called once per frame
function Update () {
}

static function FindGameObjectsWithLayer (layer : int) : GameObject[] {
     var goArray = FindObjectsOfType(GameObject);
     var goList = new System.Collections.Generic.List.<GameObject>();
     for (var i = 0; i < goArray.Length; i++) {
         if (goArray[i].layer == layer) {
             goList.Add(goArray[i]);
         }
     }
     if (goList.Count == 0) {
         return null;
     }
     return goList.ToArray();
 }