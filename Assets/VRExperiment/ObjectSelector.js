﻿#pragma strict

var layermask : LayerMask;
function Start () {
}

function Update () {
	ResetHighlights();
	// check if some object has been positively selected, if so don't preHighlight any other objects
	PrehighlightObjectInCenter();
}

function PrehighlightObjectInCenter(){
	var hit : RaycastHit;
 	var cameraCenter = GetComponent.<Camera>().ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, GetComponent.<Camera>().nearClipPlane));
	var fwd: Vector3 = transform.TransformDirection(Vector3.forward);

	var target_layerIndex = LayerMask.NameToLayer("Selectable");
	var targets = UsefulUtils.FindGameObjectsWithLayer(target_layerIndex);

	var anySelected = false;
	for (var i = 0; i < targets.Length; i++){
		var script : Selectable = targets[i].GetComponent(Selectable) as Selectable;
		if (script.selected){
			anySelected=true;
			break;
		}
	}

	if(Physics.Raycast(transform.position, fwd, hit, 10000000, layermask)){
		var objectInCenter = hit.collider.gameObject;
		objectInCenter.SendMessage("focus", anySelected);
	}
}

function ResetHighlights(){
	var target_layerIndex = LayerMask.NameToLayer("Selectable");
	var targets = UsefulUtils.FindGameObjectsWithLayer(target_layerIndex);
	for (var i = 0; i < targets.Length; i++) {
		targets[i].SendMessage("unfocus");
	}
}


function ApproveSelection(){
	print("Gesture \"Approval\" recognized.");

	var target_layerIndex = LayerMask.NameToLayer("Selectable");
	var targets = UsefulUtils.FindGameObjectsWithLayer(target_layerIndex);

	var anySelected = false;

	for (var j = 0; j < targets.Length; j++){
		var script : Selectable = targets[j].GetComponent(Selectable) as Selectable;
		if (script.selected){
			anySelected=true;
			break;
		}
	}

	if(!anySelected){
		for (var i = 0; i < targets.Length; i++) {
			targets[i].SendMessage("ApproveSelection");
		}
	}
}

function Deselect(){
	print("Gesture \"Approval\" recognized.");
	var target_layerIndex = LayerMask.NameToLayer("Selectable");
	var targets = UsefulUtils.FindGameObjectsWithLayer(target_layerIndex);

	for (var i = 0; i < targets.Length; i++){
		var script : Selectable = targets[i].GetComponent(Selectable) as Selectable;
		if (script.selected){
			targets[i].SendMessage("Deselect");
			break;
		}
	}
}