﻿using UnityEngine;
using Leap.Unity;
namespace VRExperiment.Unity {

  /// <summary>
  /// Use this component on a Game Object to allow it to be manipulated by a pinch gesture.  The component
  /// allows rotation, translation, and scale of the object (RTS).
  /// </summary>
  public class RTRotation : MonoBehaviour {

    public enum RotationMethod {
      None,
      Single,
      Full
    }

    [SerializeField]
    private FistDetector _fistDetectorA;
    public FistDetector FistDetectorA
        {
      get {
        return _fistDetectorA;
      }
      set {
        _fistDetectorA = value;
      }
    }

    [SerializeField]
    private FistDetector _fistDetectorB;
    public FistDetector FistDetectorB
        {
      get {
        return _fistDetectorB;
      }
      set {
        _fistDetectorB = value;
      }
    }

    [SerializeField]
    private RotationMethod _oneHandedRotationMethod;

    [SerializeField]
    private RotationMethod _twoHandedRotationMethod;

    [SerializeField]
    private bool _allowScale = true;

    [Header("GUI Options")]
    [SerializeField]
    private KeyCode _toggleGuiState = KeyCode.None;

    [SerializeField]
    private bool _showGUI = true;
	
	private bool _startedGrabbing = false;
	private Quaternion _startingRotObj = Quaternion.identity;

    private Transform _anchor;

    private float _defaultNearClip;

    void Start() {
//      if (_pinchDetectorA == null || _pinchDetectorB == null) {
//        Debug.LogWarning("Both Pinch Detectors of the LeapRTS component must be assigned. This component has been disabled.");
//        enabled = false;
//      }

      GameObject pinchControl = new GameObject("RTS Anchor");
      _anchor = pinchControl.transform;
      _anchor.transform.parent = transform.parent;
      transform.parent = _anchor;
    }

    void Update() {
      if (Input.GetKeyDown(_toggleGuiState)) {
        _showGUI = !_showGUI;
      }

      bool didUpdate = false;
      if(_fistDetectorA != null)
        didUpdate |= _fistDetectorA.DidChangeFromLastFrame;
      if(_fistDetectorB != null)
        didUpdate |= _fistDetectorB.DidChangeFromLastFrame;

      if (didUpdate) {
        transform.SetParent(null, true);
      }

			if( this.GetComponent<Selectable>().selected) {
        if (_fistDetectorA != null && _fistDetectorA.IsActive &&
		  _fistDetectorB != null && _fistDetectorB.IsActive) {
		    transformDoubleAnchor ();
			} else if (_fistDetectorA != null && _fistDetectorA.IsActive) {
					transformSingleAnchor (_fistDetectorA);
				} else if (_fistDetectorB != null && _fistDetectorB.IsActive) {
					transformSingleAnchor (_fistDetectorB);
				}
				else if (_fistDetectorB != null && !_fistDetectorB.IsActive) {
					_startedGrabbing = false;
				} 
			}
      if (didUpdate) {
        transform.SetParent(_anchor, true);
      }
    }

    void OnGUI() {
      if (_showGUI) {
        GUILayout.Label("One Handed Settings");
        doRotationMethodGUI(ref _oneHandedRotationMethod);
        GUILayout.Label("Two Handed Settings");
        doRotationMethodGUI(ref _twoHandedRotationMethod);
        _allowScale = GUILayout.Toggle(_allowScale, "Allow Two Handed Scale");
      }
    }

    private void doRotationMethodGUI(ref RotationMethod rotationMethod) {
      GUILayout.BeginHorizontal();

      GUI.color = rotationMethod == RotationMethod.None ? Color.green : Color.white;
      if (GUILayout.Button("No Rotation")) {
        rotationMethod = RotationMethod.None;
      }

      GUI.color = rotationMethod == RotationMethod.Single ? Color.green : Color.white;
      if (GUILayout.Button("Single Axis")) {
        rotationMethod = RotationMethod.Single;
      }

      GUI.color = rotationMethod == RotationMethod.Full ? Color.green : Color.white;
      if (GUILayout.Button("Full Rotation")) {
        rotationMethod = RotationMethod.Full;
      }

      GUI.color = Color.white;

      GUILayout.EndHorizontal();
    }

    private void transformDoubleAnchor() {
      _anchor.position = (_fistDetectorA.Position + _fistDetectorB.Position) / 2.0f;
		//_anchor.position = (_fistDetectorA.Position);
			Vector3 initialPosition = transform.position;
      switch (_twoHandedRotationMethod) {
        case RotationMethod.None:
          break;
        case RotationMethod.Single:
          Vector3 p = _fistDetectorA.Position;
          p.y = _anchor.position.y;
          _anchor.LookAt(p);
          break;
			case RotationMethod.Full:
				Quaternion pp = Quaternion.Lerp (_fistDetectorA.Rotation, _fistDetectorB.Rotation, 0.5f);
				Vector3 u = pp * Vector3.up;
				_anchor.LookAt(_fistDetectorA.Position, u);
				transform.position = initialPosition;
          break;
      }

      if (_allowScale) {
        _anchor.localScale = Vector3.one * Vector3.Distance(_fistDetectorA.Position, _fistDetectorB.Position);
      }
    }

private void transformSingleAnchor(FistDetector singleFist) {
      switch (_oneHandedRotationMethod) {
        case RotationMethod.None:
        break;
        case RotationMethod.Single:
        Vector3 p = singleFist.Rotation * Vector3.right;
        p.y = _anchor.position.y;
        _anchor.LookAt(p);
        break;
			case RotationMethod.Full:

		// Standard way
		//transform.rotation = singleFist.Rotation;
		//transform.rotation = transformStartRotation + fistRotation - fistStartRotation
		Quaternion startingRotFist = singleFist.StartingRotation;
		if (startingRotFist != Quaternion.identity && !_startedGrabbing) {
			_startedGrabbing = true;
			_startingRotObj = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
			print ("Started grabbing object with starting rotation: " + _startingRotObj.ToString ());
			print ("Case1");
		}
					
		if (startingRotFist == Quaternion.identity && _startedGrabbing) {
			print ("Case2");
			_startedGrabbing = false;
		}

		if (_startedGrabbing) {
			print ("Case3");
			Quaternion rotChange =  Quaternion.Inverse(startingRotFist) * singleFist.Rotation;
			
			Quaternion targetRotation = _startingRotObj * rotChange;
					
					// For quaternion type update
					//transform.rotation = targetRotation;

					// For euler angles instead of quaternion
					//Vector3 targetRotation_euler = Vector3.Lerp (_startingRotObj.eulerAngles, targetRotation.eulerAngles, 1);
					//transform.eulerAngles = targetRotation_euler;
		}
				//print ("ThislastRotationIs: " + lastRotation.ToString ());
				//print ("RotGrad is: " + singlePinch.RotGrad.ToString ());
				/*
				Vector3 rotGrad = singlePinch.Rotation.eulerAngles - singlePinch.lastRotation;
				transform.Rotate (rotGrad);
				print ("RotGrad is: " + rotGrad.ToString ());
				print ("ThisRotationIs: " + singlePinch.Rotation.eulerAngles.ToString ());
				print ("LastActiveRotationWas: " + singlePinch.lastRotation.ToString());
				*/
				//Quaternion rotGrad = singlePinch.Rotation * Quaternion.Inverse(singlePinch.LastActiveRotation);
				//transform.rotation =  transform.rotation * rotGrad;
          		//_anchor.rotation = singlePinch.Rotation;
				//_anchor.Rotate(Vector3.right*Time.deltaTime);
				//_anchor.rotation = singlePinch.Rotation;
          break;
      }
      //_anchor.localScale = Vector3.one;
    }
  }
}